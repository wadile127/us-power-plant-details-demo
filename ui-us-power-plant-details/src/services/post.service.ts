import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FilterInfo } from '../app/app.component';
  
@Injectable({
  providedIn: 'root'
})
export class PostService {
  private url = 'http://localhost:3000/powerplant/adnoc/plantDetails/readTopNState';
  private stateUrl = 'http://localhost:3000/powerplant/adnoc/plantDetails/readStates';
   
  constructor(private httpClient: HttpClient) { }
  
  getPlantsDetails(data:FilterInfo){
    let filterUrl = `${this.url}?topN=${data.topPlants}`;
    if(data.state) {
      filterUrl =  `${filterUrl}&state=${data.state}`
    }
    return this.httpClient.get(filterUrl);
  }

  getAllPlants() {
    return this.httpClient.get(this.url);
  }

  getStates() {
    return this.httpClient.get(this.stateUrl);
  }
  
}