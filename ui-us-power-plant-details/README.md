# ui-us-power-plant-details

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.3.0.

## Project Objectives: 
This codebase was created to demonstrate a fully fledged full-stack application built with Spring boot + Angular 13 and MongoDB including plant operations, top plants and more.

## Prerequisites:
- Angular 13
- node-v16.14.2-x64
- Clone project from [Git lab link](https://gitlab.com/wadile127/ui-us-power-plant-details)

## Cloning & Starting Project locally:
Please run server side project first by reffering [Git lab link](https://gitlab.com/wadile127/us-power-plant-details)

a) once you clone the angular project run **npm install** command.  
b) Run the angular project using **ng serve**.

## Application Test report:
Please refer **US Power Plant Details Demo Project.docx** present inside /src/doc


  