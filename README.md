# US-Power-Plant-Details-Demo

## Project Objectives: 
This codebase was created to demonstrate a fully fledged full-stack application built with Angular 13, Node JS and MongoDB including plant operations, top plants and more.

## Prerequisites: 
**backend-us-power-plant-details (Node JS backend project)** 

Your system needs to have the following minimum requirements to run a Node JS project:    
a) node-v16 (lower also works)  
b) MongoDB 4.4.3 Community [should be UP and running on port 27017]    
c) Please import json data in Mongo DB using below mentioned commands    
i) mongoimport --db PowerPlantDetails --collection USStateDetails --file <your-dir-path/USStateDetails.json> --jsonArray  

**E.g** mongoimport --db PowerPlantDetails --collection USStateDetails --file D:/us-power-plant-details-demo/backend-us-power-plant-details/jsondata/USStateDetails.json --jsonArray  

ii) mongoimport --db PowerPlantDetails --collection plantDetails --file <your-dir-path/plantDetails.json> --jsonArray  

**E.g** mongoimport --db PowerPlantDetails --collection plantDetails --file D:/us-power-plant-details-demo/backend-us-power-plant-details/jsondata/plantDetails.json --jsonArray 
  
**Note that**, please execute above command from bin directory of MongoDB  

d) Clone project from [Git lab link](https://gitlab.com/wadile127/us-power-plant-details-demo)

#### Cloning & Starting Project Locally  

Please refer **US Power Plant Details Demo Project.docx** present inside **us-power-plant-details-demo/doc directory**

##### Server side (backend-us-power-plant-details (Node JS backend project))  
a) Run/start up mongbo db service (It should run on 27017).  
b) npm install  
c) npm run build  
d) npm run start

Once you execute above commands then mongo repo and backend server will be ready to use by our applications.   

##### Client side (ui-us-power-plant-details (Angular 13))    
a) npm install  
b) ng serve 

Once you execute above commands then client  will be ready to use at **http://localhost:4200/**.  

### Application Test report:  
Please refer **US Power Plant Details Demo Project.docx** present inside /resources/doc  

### Future Enhancements:  
- CSV file uploading base on scheduler.  
- Need to apply rate limiting policy on server side project as its develop for non-authenticate users.  
- Deploy the both the applications either on AWS/GCP or AZURE cloud.
- CI/CD integration along with container management using Kubernetes.
- Secure the application using gateway.

