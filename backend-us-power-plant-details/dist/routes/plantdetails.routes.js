"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const states_controller_1 = require("../controllers/states.controller");
const cors_1 = __importDefault(require("cors"));
const plantdetails_controller_1 = require("../controllers/plantdetails.controller");
const router = (0, express_1.Router)();
router.get("/readStates", (0, cors_1.default)({ origin: '*' }), states_controller_1.renderStates);
router.get("/readTopNState", (0, cors_1.default)({ origin: '*' }), plantdetails_controller_1.renderPlantDetails);
exports.default = router;
