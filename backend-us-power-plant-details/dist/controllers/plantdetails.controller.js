"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.renderPlantDetails = void 0;
const plantDetails_model_1 = __importDefault(require("../models/plantDetails.model"));
/**
 * This is used to render plant details
 * @param req
 * @param res
 */
const renderPlantDetails = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { topN, state } = req.query;
    let params = {};
    const limit = Number(topN) > 0 ? Number(topN) : 50;
    // sort in descending (-1) order by plantAnnualNetGeneration
    const sort = { plantAnnualNetGeneration: -1 };
    if (state && state !== '') {
        params.plantStateAbbreviation = state;
    }
    ;
    const plantDetails = yield plantDetails_model_1.default.find(params).sort(sort).limit(limit).lean();
    res.status(200).json(plantDetails);
});
exports.renderPlantDetails = renderPlantDetails;
