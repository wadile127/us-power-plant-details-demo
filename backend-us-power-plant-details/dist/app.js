"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Applicaction = void 0;
const express_1 = __importDefault(require("express"));
const config_1 = require("./config");
// Routes
const plantdetails_routes_1 = __importDefault(require("./routes/plantdetails.routes"));
class Applicaction {
    constructor() {
        this.app = (0, express_1.default)();
        this.routes();
    }
    routes() {
        this.app.use("/powerplant/adnoc/plantDetails", plantdetails_routes_1.default);
    }
    start() {
        this.app.listen(config_1.PORT, () => {
            console.log("Server is running at", config_1.PORT);
        });
    }
}
exports.Applicaction = Applicaction;
