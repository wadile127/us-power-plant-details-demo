import { Request, Response } from "express";
import States from "../models/states.model";

/**
 * This is used to render states.
 * @param req 
 * @param res 
 */
export const renderStates = async (req: Request, res: Response) => {
  const states = await States.find().lean();
  res.status(200).json(states);
};

