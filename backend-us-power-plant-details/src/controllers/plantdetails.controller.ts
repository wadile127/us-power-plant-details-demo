import { Request, Response } from "express";
import PlantDetails from "../models/plantDetails.model";
import cors from 'cors';
/**
 * This is used to render plant details
 * @param req 
 * @param res 
 */
export const renderPlantDetails = async (req: Request, res: Response) => {
  const { topN, state} = req.query;

  let params:any={};
  const limit = Number(topN) > 0 ? Number(topN) : 50;

// sort in descending (-1) order by plantAnnualNetGeneration
const sort = { plantAnnualNetGeneration: -1 };

  if(state && state !== '') {
    params.plantStateAbbreviation = state;
  };
  const plantDetails = await PlantDetails.find(params).sort(sort).limit(limit).lean();
  res.status(200).json(plantDetails);
};