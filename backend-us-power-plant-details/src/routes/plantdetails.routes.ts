import { Router, Request, Response } from "express";
import {
  renderStates
} from "../controllers/states.controller";
import cors from 'cors';

import {renderPlantDetails} from "../controllers/plantdetails.controller"
const router = Router();

router.get("/readStates", cors({origin: '*'}), renderStates);
router.get("/readTopNState", cors({origin: '*'}), renderPlantDetails);

export default router;  
