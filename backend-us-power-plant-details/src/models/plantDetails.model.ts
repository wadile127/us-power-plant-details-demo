import { getModelForClass, ModelOptions, prop } from "@typegoose/typegoose";

@ModelOptions({
  schemaOptions: {
    timestamps: true,
    collection: 'plantDetails'
  },
})
export class PlantDetails {
  @prop({ type: String, required: true })
  public year: string;

  @prop({ type: String, required: true })
  public plantStateAbbreviation: string;

  @prop({ type: String, required: true })
  public plantName: string;

  @prop({ type: Number, required: true })
  public plantLatitude: Number;

  @prop({ type: Number, required: true })
  public plantLongitude: Number;

  @prop({ type: Number, required: true })
  public plantAnnualNetGeneration: Number;

  @prop({ type: String, required: true })
  public plantTotalNonrenewablesGenePercent: string;

  @prop({ type: String, required: true })
  public PlantTotalRenewablesGenPer: string;

  @prop({ type: String, required: true })
  public PlantTotalNonhydroRenewablesGenPercent: string;

  @prop({ type: String, required: true })
  public plantTotalCombustionGenerationPercent: string;

  @prop({ type: String, required: true })
  public PlantTotalNoncombustionGenPercent: string;
}

const PlantDetailsModel = getModelForClass(PlantDetails);

export default PlantDetailsModel;
