import { getModelForClass, ModelOptions, prop } from "@typegoose/typegoose";

@ModelOptions({
  schemaOptions: {
    timestamps: true,
   collection: 'USStateDetails'
  },
})
export class States {
  @prop({ type: String, lowercase: true, required: true })
  public stateName: string;

  @prop({ type: String, lowercase: true, required: true })
  public usAbbreviation: string;
}

const StatesModel = getModelForClass(States);

export default StatesModel;
