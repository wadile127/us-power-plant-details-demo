import express from "express";
import morgan from "morgan";
import { create } from "express-handlebars";
import path from "path";
import { PORT } from "./config";

// Routes
import plantDetailsRoutes from "./routes/plantdetails.routes"

export class Applicaction {
  app: express.Application;

  constructor() {
    this.app = express();
    this.routes();
  }

  routes() {
    this.app.use("/powerplant/adnoc/plantDetails", plantDetailsRoutes); 
  }

  start(): void {
    this.app.listen(PORT, () => {
      console.log("Server is running at", PORT);
    });
  }

  
}
